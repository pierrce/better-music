package gg.artifice.bettermusic.config;

import net.minecraft.util.ResourceLocation;

public class ResourceLocations {
	
	public static ResourceLocation edenLocation;
	
	public static void Initialize() {
		
		edenLocation = new ResourceLocation("bettermusic", "eden");
	}
}
