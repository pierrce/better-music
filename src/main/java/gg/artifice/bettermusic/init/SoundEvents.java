package gg.artifice.bettermusic.init;

import gg.artifice.bettermusic.config.ResourceLocations;
import net.minecraft.client.audio.SoundManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;

public class SoundEvents {
	
	/* All Sound Events */
	public static SoundEvent edenSoundEvent;
	
	public static void Initialize() {
		
		ResourceLocations.Initialize();
		
		edenSoundEvent = new SoundEvent(ResourceLocations.edenLocation);
	}
}
