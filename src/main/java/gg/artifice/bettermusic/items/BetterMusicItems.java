package gg.artifice.bettermusic.items;

import gg.artifice.bettermusic.BetterMusic;
import gg.artifice.bettermusic.init.SoundEvents;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemRecord;
import net.minecraftforge.client.event.sound.SoundEvent;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class BetterMusicItems {

	public static final ItemRecord ItemEdenRecord = null;
	
	@EventBusSubscriber(modid = BetterMusic.MODID)
	public static class RegistrationHandler {
		
		@SubscribeEvent 
		public static void registerItems(Register<Item> event) {
			
			SoundEvents.Initialize();
			
			final Item[] items = {new ItemEdenDisc("Eden", CreativeTabs.MISC, SoundEvents.edenSoundEvent).setRegistryName(BetterMusic.MODID)};
			
			event.getRegistry().registerAll(items);
		}
	}
}
