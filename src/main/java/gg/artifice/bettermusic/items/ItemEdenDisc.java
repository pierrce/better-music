package gg.artifice.bettermusic.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemRecord;
import net.minecraft.util.SoundEvent;

public class ItemEdenDisc extends ItemRecord {

	protected ItemEdenDisc(String name, CreativeTabs tab, SoundEvent soundIn) {
		super(name, soundIn);
		setCreativeTab(tab);
		setUnlocalizedName(name);
		setRegistryName(name);
	}
}
